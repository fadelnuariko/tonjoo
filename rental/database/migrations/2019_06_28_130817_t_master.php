<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_master', function (Blueprint $table) {
            $table->increments('id_master');
            $table->string('description',200);
            $table->string('code',6);
            $table->float('rate_euro',8,2);
            $table->date('date_paid');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_master');
    }
}
