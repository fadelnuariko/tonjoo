<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('content.dashboard');
});

Route::get('/dashboard', function () {
    return view('content.dashboard');
});

Route::GET("/data/tambah", "DataController@tambah");
Route::GET("/data/list", "DataController@list");
Route::GET("/data/rekap", "DataController@rekap");

