@extends('layouts.main')

@section('link_breadcrumb')
/dashboard
@endsection

@section('parent_breadcrumb')
Dashboard
@endsection

@section('active_breadcrumb')
List Data
@endsection

@section('page_title')
List Data
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            List Data Transaksi
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <button class="btn btn-success"> Tambah Transaksi</button>
                </div>
                <div class="col-md-10 ">
                    <div class="float-right">
                        <div class="row mb-3">
                            <div class="mr-3">
                                <input type="date" class="form-control" id="date" placeholder="date">
                            </div>
                            to
                            <div class="ml-3">
                                <input type="date" class="form-control" id="date" placeholder="date">
                            </div>
                            <div class="ml-3">
                                <select name="" class="form-control" id="">
                                    <option value="">Income</option>
                                    <option value="">Expense</option>
                                </select>
                            </div>
                            <div class="ml-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend"><i
                                                class="fa fa-search"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="validationCustomUsername"
                                        placeholder="Search" aria-describedby="inputGroupPrepend" required>

                                </div>


                            </div>
                            <div class="ml-2">
                                <button class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Deskripsi</th>
                            <th>Code</th>
                            <th>Rate Euro</th>
                            <th>Date Paid</th>
                            <th>Kategori</th>
                            <th>Nama Transaksi</th>
                            <th>Nominal(idr)</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Test</td>
                            <td>123</td>
                            <td>15000</td>
                            <td>11 Agustus 2019</td>
                            <td>Income</td>
                            <td>Rental Mobil bulan agustus</td>
                            <td>Nominal</td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Test</td>
                            <td>123</td>
                            <td>15000</td>
                            <td>11 Agustus 2019</td>
                            <td>Income</td>
                            <td>Rental Mobil bulan agustus</td>
                            <td>Nominal</td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Test</td>
                            <td>123</td>
                            <td>15000</td>
                            <td>11 Agustus 2019</td>
                            <td>Income</td>
                            <td>Rental Mobil bulan agustus</td>
                            <td>Nominal</td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Test</td>
                            <td>123</td>
                            <td>15000</td>
                            <td>11 Agustus 2019</td>
                            <td>Income</td>
                            <td>Rental Mobil bulan agustus</td>
                            <td>Nominal</td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Test</td>
                            <td>123</td>
                            <td>15000</td>
                            <td>11 Agustus 2019</td>
                            <td>Income</td>
                            <td>Rental Mobil bulan agustus</td>
                            <td>Nominal</td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-5">
                    <div class="row">
                        <select name="" class="form-control mr-2" id="" style="width:80px;">
                            <option value="">5</option>
                            <option value="">10</option>
                            <option value="">20</option>
                        </select><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Menampilkan 1 dari 10 Data</div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="float-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">

                            <ul class="pagination">
                                <li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                <li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">2</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">3</a></li>
                                <li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">4</a></li>
                                <li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0" class="page-link">Next</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    @endsection
