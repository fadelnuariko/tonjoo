@extends('layouts.main')

@section('link_breadcrumb')
/dashboard
@endsection

@section('parent_breadcrumb')
Dashboard
@endsection

@section('active_breadcrumb')
Tambah Data
@endsection

@section('page_title')
Tambah Data
@endsection

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Input Data Transaksi
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="description" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <textarea name="" class="form-control" id="description" cols="30" rows="6"
                                placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="code" class="col-sm-2 col-form-label">Code</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="code" placeholder="Code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rate" class="col-sm-2 col-form-label">Rate Euro</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rate" placeholder="Rate Euro">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Date</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="date" placeholder="date">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    Data Transaksi
                </div>
                <div class="card-body">
                    <div class="card" >
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-2 col-form-label">Category</label>
                                        <div class="col-sm-9">
                                            <select name="" class="form-control" id="">
                                                <option value="">Income</option>
                                                <option value="">Expense</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-9">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <th>Nama Transaksi</th>
                                            <th>Nominal</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Rental Mobil</td>
                                                <td>20000</td>
                                            </tr>
                                            <tr>
                                                <td>Rental Mobil</td>
                                                <td>20000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-1">
                                    <a href="javascript:void(0)" class="btn btn-primary" style="bottom:15px; position:absolute"><i class="fas fa-fw fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="container">
                          <button class="btn btn-primary float-right mt-2 ">Tambah</button>   
                        </div>  
                      </div>

                      <br>
                      <div class="card" >
                          <div class="card-body">
                              <div class="row">
                                  <div class="col-sm-12">
                                      <div class="form-group row">
                                          <label for="rate" class="col-sm-2 col-form-label">Category</label>
                                          <div class="col-sm-9">
                                              <select name="" class="form-control" id="">
                                                  <option value="">Income</option>
                                                  <option value="">Expense</option>
                                              </select>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2">
                                  </div>
                                  <div class="col-sm-9">
                                      <table class="table table-striped table-bordered table-hover">
                                          <thead>
                                              <th>Nama Transaksi</th>
                                              <th>Nominal</th>
                                          </thead>
                                          <tbody>
                                              <tr>
                                                  <td>Rental Mobil</td>
                                                  <td>20000</td>
                                              </tr>
                                              <tr>
                                                  <td>Rental Mobil</td>
                                                  <td>20000</td>
                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>
                                  <div class="col-sm-1">
                                      <a  href="javascript:void(0)" class="btn btn-primary" style="bottom:15px; position:absolute"><i class="fas fa-fw fa-plus"></i></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="container">
                            <button class="btn btn-primary float-right mt-2 ">Tambah</button>   
                          </div>  
                        </div>
                </div>
                
            </div>
            <div class="row">
                <div class="container">
                  <button class="btn btn-primary float-right mt-2 ">Simpan</button>   
                </div>  
              </div>
              <br>
        </div>
    </div>
    <br>
    @endsection
