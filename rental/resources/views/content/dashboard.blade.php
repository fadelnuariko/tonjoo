@extends('layouts.main')

@section('link_breadcrumb')
    dashboard
@endsection

@section('parent_breadcrumb')
    Dashboard
@endsection

@section('active_breadcrumb')
    Dashboard
@endsection
@section('page_title')
    Dashboard
@endsection


@section('content')
<div class="col-md-12">
    <div class="card">
    <img class="card-img-top img-fluid img-thumbnail" src="{{URL::asset('assets/img/mokey.jpg')}}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Selamat Datang</h5>
          <p class="card-text">Jangan lupa tersenyum</p>
          <a href="#" class="btn btn-primary">Buka Master Data</a>
        </div>
      </div>
</div>
<br>
@endsection