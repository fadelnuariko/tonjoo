<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function tambah(){
        return view('content.tambah_data');
    }
    public function list(){
        return view('content.list_data');
    }
    public function rekap(){
        return view('content.rekap_data');
    }
}
