$(document).ready(function(){

  $(document).on('click', '.edit-data', function(){
    $('.judul').html('Edit Data');
    var user_id = $(this).attr('data-id');
    console.log(user_id);
    $.ajax({
      type:'GET',
      url:'/data/edit/'+user_id,
      success:function(data){
         $('.btn-save').val("edit");
          $('.label-nama_sales').addClass('active');
          $('.label-nama_konsumen').addClass('active');
          $('.label-alamat_konsumen').addClass('active');
          $('.label-hp_konsumen').addClass('active');
          $('#user_id').val(data.data.id);
          
          $('#nama_sales').val(data.data.nama_sales);
          $('#nama_konsumen').val(data.data.nama_konsumen);
          $('#alamat_konsumen').val(data.data.alamat_konsumen);
          $('#hp_konsumen').val(data.data.hp_konsumen);
      }
   });
  });
   
  $(".btn-save").click(function(e){
    e.preventDefault();

    var btn = $('.btn-save').val();
    var id = $("input[name=user_id]").val();
    var nama_sales = $("input[name=nama_sales]").val();
    var nama_konsumen = $("input[name=nama_konsumen]").val();
    var alamat_konsumen = $("input[name=alamat_konsumen]").val();
    var hp_konsumen = $("input[name=hp_konsumen]").val();
    
    if(btn=="edit"){
      if(nama_sales=="" || nama_konsumen==""|| alamat_konsumen=="" || hp_konsumen=="" ){
        Swal.fire('Kesalahan','Masukan inputan dengan benar','error');
      }else{
        $.ajax({
          data: $('#formData').serialize(),
          type:'POST',
          url:'/data/edit',
          data:{id:id, nama_sales:nama_sales, nama_konsumen:nama_konsumen, alamat_konsumen:alamat_konsumen, hp_konsumen:hp_konsumen},
          success:function(data){
             if(data.status=="OK"){
              var user = '<tr id="tr_' + data.data.id + '"><td class="center">' + data.data.id + '</td><td>' + data.data.nama_sales + '</td><td>' + data.data.nama_konsumen + '</td>'+ '</td><td>' + data.data.alamat_konsumen + '</td>'+ '</td><td>' + data.data.hp_konsumen + '</td>';
              user += '<td><a href="javascript:void(0)"  data-id="'+data.data.id+'" class="waves-effect waves-light btn blue darken-2 edit-data  modal-trigger " data-target="modal1"><i class="material-icons ">edit</i></a> <a data-id="' + data.data.id + '" class="waves-effect waves-light btn red darken-2 delete-data"><i class="material-icons ">delete</i></a></td>';
              
              $('#tr_'+data.data.id).replaceWith(user); // Add to table

              $('.modal.open').modal('close'); // Close modal

              Swal.fire('Success','Data berhasil update','success'); // Alert sukses 

              $('#formData').trigger("reset"); //Reset Form

             }else{
              Swal.fire('Unknown','Telah terjadi kesalahan','error');  
             }
          }
       });
      }
    }
    
  });
});