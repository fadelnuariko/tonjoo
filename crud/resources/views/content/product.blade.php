@extends('layouts.main')
 
@section('content')
<script>
$(document).ready( function () {
    $('#product-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'product/json',
        columns: [
            { data: 'kode_minat', name: 'kode_minat' },
            { data: 'nama_motor', name: 'nama_motor' },
            { data: 'status', name: 'status' },
            { data: 'leasing', name: 'leasing' }
        ]
    });
} );
</script>
<div class="container">
    <div class="row">
        <div class="col s12">
          <div class="card">
            
            <div class="card-content">
    <table class="table table-bordered" id="product-table">
        <thead>
            <tr>
                <th>Kode Minat</th>
                <th>Motor</th>
                <th>Status</th>
                <th>Leasing</th>
            </tr>
        </thead>
    </table>
    </div>
          </div>
        </div>
      </div>
</div>
@stop
 

