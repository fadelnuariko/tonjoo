{{-- Nav topbar --}}
<nav>
    <div class="nav-wrapper">
        <a href="#" class="brand-logo"><strong>Bintang Motor</strong></a>
        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li class="{{ request()->is('data') ? 'active' : '' }}"><a href="{{url('/data')}}"><i class="material-icons left">list</i>Customer</a></li>
            <li class="{{ request()->is('product') ? 'active' : '' }}"><a href="{{url('/product')}}"><i class="material-icons left">motorcycle</i>Product</a></li>
        </ul>
    </div>
</nav>

{{-- Nav responsive --}}
<ul id="slide-out" class="sidenav">
    <li class="{{ request()->is('data') ? 'active' : '' }}"><a href="{{url('/data')}}">Customer</a></li>
    <li class="{{ request()->is('product') ? 'active' : '' }}"><a href="{{url('/product')}}">Product</a></li>
</ul>
