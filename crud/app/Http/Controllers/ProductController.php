<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DataTables;

class ProductController extends Controller
{
    public function json(){
        return Datatables::of(Product::all())->make(true);
    }
    public function index(){
        return view('content.product');
    }
}
